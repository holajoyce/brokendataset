# Chime Take home project
Instructions for candiate

## Problem
A developer wrote a script that accidentally messed up some data and there have also been a few bugs over the years that could have caused issues, meaning the data may have stopped adhering to the business rules above.

When casually looking through some of that members data, some issues were identified. Now we'd like to have a comprehensive analysis done on the data to find any anomalies.

## Task
Your task is to use Python (or Java/Go or any language you’re comfortable with) to *identify* the data that violates any business rule and report the anomalies. You should provide the code used to find the anomalies, and you may *report* the anomalies in any way that makes sense to you. This is similar to building a change detection system, but hopefully small enough that it isn't overly burdensome. The final solution should, at a minimum, validate data meets reasonable row- and field-level validation for all four business rules above.

### Business Rules
You must be 18 years or older upon account creation.
You must provide valid identifiers (email, zip code, phone number) during enrollment.
Members should never be removed (i.e. rows deleted), they should be marked as cancelled.
Attached is a DB schema for storing the input, the data should conform completely to the schema.

### schema
create table members (
id int not null auto_increment,
first_name varchar(255) not null,
last_name varchar(255) not null,
email varchar(255) not null,
phone int(10) not null,
status enum('active', 'cancelled') not null,
zip5 int(5) not null,
created_at datetime,
updated_at datetime,
birth_date date not null);

