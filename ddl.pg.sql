drop table if exists members cascade;
create table if not exists members (
  -- id serial -- int not null auto_increment
  id int
  , first_name varchar(255) not null,
  last_name varchar(255) not null,
  email varchar(255) not null,
  phone varchar(20) not null, -- int(10) not null,
  status varchar(10) not null,
  zip5 varchar(20) not null,  -- int(5) not null,
  created_at timestamp, -- datetime
  updated_at timestamp, -- datetime,
  birth_date date not null
);

