import urllib.request
from datetime import datetime, timezone
import json
import csv
from dateutil.parser import parse

now = datetime.now(timezone.utc).isoformat()

# anonymous function to try to parse out the json objects
# and do some transforms as needed
convert_json = lambda d: {
    k:int(v)  if (v.isdigit() and not k in ['phone','zip5'])
    else 
        parse(v).strftime("%Y-%m-%d")
        if k =="birth_date"
        else v
    for k, v in d.items()
}

def writecsv(lines):
    if not lines:
        return
    try:
        with open("to_upload/members.csv", 'w+') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=lines[0].keys(), quoting=csv.QUOTE_ALL)
            writer.writeheader()
            for data in lines:
                writer.writerow(data)
    except IOError as e:
        print("Found error writing in writing to csv: " + e)
        print("I/O error on line: " + data)

if __name__ == "__main__":

    json_list = []
    lines = []

    with open('Data_Sets.json', 'r') as json_file:
        json_list = list(json_file)

    for json_str in json_list:
        try:
            lines.append(json.loads(json_str, object_hook=convert_json))
        except Exception as e:
            with open("errors/date_errors.jsonl","a+") as f:
                f.write(json_str)
    print(len(lines))
    writecsv(lines)
