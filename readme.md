# Chime's Take home project
Finding broken Acme Data
- [Chime's Take home project](#chimes-take-home-project)
  - [How to get setup](#how-to-get-setup)
      - [Set up environment & database](#set-up-environment--database)
      - [Dump json as csv](#dump-json-as-csv)
      - [Copy files into the database](#copy-files-into-the-database)
  - [The Big Idea](#the-big-idea)
  - [The Reports](#the-reports)
  - [Relevant Code](#relevant-code)
  - [Finish](#finish)
      - [Cleanup](#cleanup)
  - [Other ideas](#other-ideas)
    - [Unused code](#unused-code)
  

## How to get setup
#### Set up environment & database
```sh
mkdir -p .data/pg;  # used as a volume by pg
python -m venv .venv;
source .venv/bin/activate;
pip install termgraph

# turn on pg, (if not already running on your laptop)
docker-compose -f docker/docker-compose.yml up -d ;  # this is somewhat slow, sorry
# install database
echo "SELECT 'CREATE DATABASE chime' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'chime')\gexec" |PGPASSWORD=none  psql -h localhost -Upostgres;
# install tables
PGPASSWORD=none psql -hlocalhost -U postgres -d chime -f ddl.pg.sql;
# install views
for f in ./views/*
do
  PGPASSWORD=none psql -hlocalhost -U postgres -d chime -f $f
done;
```

#### Dump json as csv
```sh
rm -rf errors/date_errors.jsonl to_upload/members.csv && python json_to_csv.py
```

#### Copy files into the database
```sh
read -r firstline < ./to_upload/members.csv;
echo $firstline;
PGPASSWORD=none psql -hlocalhost -U postgres -d chime -c  "COPY members ($firstline) FROM '/to_upload/members.csv' delimiter ','  CSV header;";
```

## The Big Idea
1. convert json to csv file, with some validations
2. setup a table in postgres w/ a fairly relaxed schema, this table is meant for us to analyze & validate the members data
3. copy the csv into postgres
4. generate reports as needed

## The Reports

1.  birth dates are not real dates
```sh
# these didn't pass validation
cat errors/date_errors.jsonl  
```

2. invalid members based on schema & business rules
```sh
# this table shows all the users with 2 extra columns to show 
# why they are invalid entries in the db
PGPASSWORD=none psql -hlocalhost -U postgres -d chime -c  "select * from members_invalid; ";
```

3. Missing members
```sh
# users that are not supposed to be deleted
PGPASSWORD=none psql -hlocalhost -U postgres -d chime -c  "select * from members_missing; ";
```

Number 2 and 3 are also in `example_results/*.csv` 

## Relevant Code
- json_to_csv.py
- views/*.sql

## Finish
#### Cleanup
```sh
docker-compose -f docker/docker-compose.yml down
```

## Other ideas
I had wanted to use singer-io to do the transforms, it's not worked out as well as I hopped.

### Unused code
```python
# jsonschema is unused right now since validation is done on db
schema = {
    "$id": "http://chime.com/members.schema.json",
    "title": "Members",
    "description": "Chime's members"
    ,"type":"object"
    ,"properties":   {
        "id":{"type": "integer"}
        ,"first_name":{"type":"string",  "maxLength": 255} 
        ,"last_name":{"type":"string", "maxLength": 255} 
        ,"email":{"type":"string",   "maxLength": 255} 
        ,"phone":{"type":"string"} 
        ,"status":{"type":"string"}
        ,"zip5":{"type":"string"}
        ,"created_at":{"type": "string",  "format": "date-time"}
        ,"updated_at":{"type": "string",  "format": "date-time"}
        ,"birth_date":{"type":"string", "format": "date"}
    }
    ,"required":[
        "first_name","last_name","email","phone"
        ,"status","zip5","birth_date"
    ]
}
```