create or replace view members_invalid as(
with members_invalid_age as(
    select 
        members.*
        ,(DATE_PART('year', created_at::date) - DATE_PART('year', birth_date::date)) age_when_applied
        ,'invalid_age'::text as why_invalid
    from members where 
        DATE_PART('year', created_at::date) - DATE_PART('year', birth_date::date) < 18
)
, members_invalid_email as(
	select 
		members.*
		, -1 age_when_applied
		,'invalid_email'::text as why_invalid
	from members
	where email !~ '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$' 
),
members_invalid_phone as(
	select 
		members.*
		, -1 age_when_applied
		,'invalid_phone'::text as why_invalid
	from members
	where phone !~ '^[0-9]{10}$' 
),
members_invalid_zip5 as(
	select 
		members.*
		, -1 age_when_applied
		,'invalid_zip'::text as why_invalid
	from members
	where zip5 !~ '^[0-9]{5}$' 
),
members_invalid_status as(
	select 
		m.* 
		, -1 age_when_applied
		,'invalid_status'::text as why_invalid
	from members m where not status  in ('cancelled','active')
)

select
* from members_invalid_age

union all 
select
* from members_invalid_email

union all 
select
* from members_invalid_phone

union all 
select
* from members_invalid_zip5

union all 
select
* from members_invalid_status

)

