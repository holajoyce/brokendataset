create or replace view members_missing as(
	with nums as(
		select generate_series(1,9999) id
	)
	SELECT 
		nums.id
	FROM nums 
	where nums.id not in (select id::int from members)
);

-- select * from missing_members